# GitLab Sparse Checkout

I cannot clone huge GitLab projects such as
[fdroid/fdroiddata](https://gitlab.com/fdroid/fdroiddata).
This provides a solution for me: sparse checkout!

Installation: 
```
pip install sparse
```


Nothing happens with this:
```
git-sparse clone https://<GITLAB_TOKEN>@gitlab.com/fdroid/fdroiddata
```

Work with the `master` branch.
```
sparse checkout master
```

Download a file:
```
sparse download metadata/org.congresointeractivo.elegilegi
```

Edit the file:
```
nano metadata/org.congresointeractivo.elegilegi
```

Upload the file:
```
sparse upload metadata/org.congresointeractivo.elegilegi -m"commit message"
```

